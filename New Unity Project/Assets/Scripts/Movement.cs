﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    //Variables
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    private float currentjumpSpeed;
    private float currentSpeed;
    private float currentGrav;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    public AudioSource jumpsound;
    public AudioSource speedsound;

  
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        currentjumpSpeed = jumpSpeed;
        currentSpeed = speed;
        currentGrav = gravity;
    }

    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        // is the controller on the ground?
        if (controller.isGrounded)
        {
            //Feed moveDirection with input.
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            //Multiply it by speed.
            moveDirection *= speed;
            //Jumping
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }
        //Applying gravity to the controller
        moveDirection.y -= gravity * Time.deltaTime;
        //Making the character move
        controller.Move(moveDirection * Time.deltaTime);
    
    }

  private void OnControllerColliderHit (ControllerColliderHit hit)
    {
        Debug.Log ("hi");

        switch (hit.gameObject.tag)
        {
            case "jumppad":
                jumpSpeed = 20.0F;
               // jumpsound.Play();
                break;
            case "Ground":
                jumpSpeed = currentjumpSpeed;
                speed = currentSpeed;
                gravity = currentGrav;
                break;
            case "speed":
                speed = 20.0F;
                speedsound.Play();
                break;
        }
    }

    private void OnTriggerEnter (Collider box)
    {
        switch (box.gameObject.tag)
        {
            case "gravity":
                Debug.Log("grav");
                gravity = 8.0F;
                break;
        }
    }

}