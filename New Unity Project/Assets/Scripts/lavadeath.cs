﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class lavadeath : MonoBehaviour
{   
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            SceneManager.LoadScene(0);
        }

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
    }
}
