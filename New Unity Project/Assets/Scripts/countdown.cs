﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class countdown : MonoBehaviour
{
    [SerializeField] float startTime = 120f;
    [SerializeField] TextMeshProUGUI timerText1;

    float timer1 = 0f;

    void Start()
    {
        StartCoroutine(Timer1());
    }

    private IEnumerator Timer1()
    {
        timer1 = startTime;

        do
        {
            timer1 -= Time.deltaTime;
            FormatText1();
            yield return null;
        }
        while (timer1 > 0);

        if (timer1 <= 0)
        {
            SceneManager.LoadScene(0);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void FormatText1()
    {
        int minutes = (int)(timer1 / 60) % 60;
        int seconds = (int)(timer1 % 60);

        timerText1.text = "";
        if (minutes > 0) { timerText1.text += minutes + ":"; }
        if (seconds >= 10) { timerText1.text += seconds; }
        if (seconds < 10) { timerText1.text += "0" + seconds; }
    }
}
